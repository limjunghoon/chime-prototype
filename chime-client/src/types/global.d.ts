declare global {
  interface Window {
    [key: string]: any;
    controllerEnvironment?: Promise<DeviceEnvironment>;
    deviceEnvironment?: Promise<DeviceEnvironment>;
  }
}
