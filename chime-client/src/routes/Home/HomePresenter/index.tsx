import React, {useState} from "react";
import './index.css';

import { Link } from 'react-router-dom';
import Button from "@material-ui/core/Button";
import Profile from "../../../components/Profile";
import { Participant } from "../../../context/MeetingProvider/types";
import Video from "../../../components/Video/VideoContainer";
import VoiceChatIcon from '@material-ui/icons/VoiceChat';

interface IProps {
  title: string;
  participants: Participant[];
  toggleVideo: any;
}

const HomePresenter: React.FC<IProps> = ({ title, participants, toggleVideo }) => {
  const [active, setActive] = useState(0);

  const profiles = participants.map(({ attIdx, memIdx, attendeeInfo, profile }, key) => (
    <Profile
      key={key}
      attIdx={attIdx}
      memIdx={memIdx}
      attendeeInfo={attendeeInfo}
      profile={profile}
      isActive={key === active}
      changeActive={() => setActive(key)}
    />
  ));

  const info = !participants.length ? '' : (
    <div className="msg messageReceived">
      { (!participants[active] || !participants[active].profile)
        ? '열람 권한이 없습니다.'
        : (
          `아이디: ${participants[active].profile.id}
          
          이름: ${participants[active].profile.name}
           
          휴대폰: ${participants[active].profile.phone}
        `)
      }
    </div>
  );

  return (
    <section className="mainApp">

      {/* user infos */}
      <div className="leftPanel">
        <header>
          <button className="trigger">
            <svg viewBox="0 0 24 24">
              <path d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z"/>
            </svg>
          </button>

          <input className="searchChats" type="search" placeholder="Search..."/>
        </header>
        <div className="chats">
          {profiles}
        </div>
      </div>

      {/* chats */}
      <div className="rightPanel">
        <div className="topBar">
          <div className="rightSide">
            <button className="tbButton search">
              <i className="material-icons">&#xE8B6;</i>
            </button>
            <button className="tbButton otherOptions">
              <i className="material-icons">more_vert</i>
            </button>
          </div>

          <div className="leftSide">
            <p className="chatName">Renan Mayrinck <span>@Grisson</span></p>
            <p className="chatStatus">Online</p>
          </div>
        </div>

        <div className="convHistory userBg">
          {info}
        </div>

        <div className="replyBar">
          <button className="attach">
            <i className="material-icons d45">attach_file</i>
          </button>

          <input type="text" className="replyMessage" placeholder="Type your message..."/>

          <div className="otherTools">
            <button className="toolButtons emoji">
              <i className="material-icons">face</i>
            </button>

            <button className="toolButtons audio" onClick={toggleVideo}>
              {/*<i className="material-icons">mic</i>*/}
              <VoiceChatIcon/>
            </button>

          </div>
        </div>
      </div>
    </section>
  );
};

export default HomePresenter;