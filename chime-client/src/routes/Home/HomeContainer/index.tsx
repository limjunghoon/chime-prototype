import React, {useState, useEffect, useReducer} from "react";
import {useHistory} from "react-router-dom";
import HomePresenter from "../HomePresenter";
import {useAuthProviderState} from "../../../context/AuthProvider";
import {useMeetingProviderDispatch, useMeetingProviderState} from "../../../context/MeetingProvider";
import { ActionType } from "../../../context/MeetingProvider/types";
import MeetingManager from "../../../lib/MeetingManager";
import reducer from "../../../components/Video/VideoContainer/reducer";
import {ScreenMessageDetail} from "amazon-chime-sdk-js";
import {DeviceMessage} from "../../../index";
import {TEST_SESSION} from "../../../../../chime-server/src/config";

const {
  JoinMeeting,
  EnterMeeting,
  StartScreenShareView,
  StopScreenShareView,
  StartLocalVideo,
  StopLocalVideo,
  TileUpdated,
  TileDeleted
} = ActionType;

const HomeContainer: React.FC = () => {
  const { authenticate, info } = useAuthProviderState();
  const { activeMeeting, participants, isSharingLocalVideo } = useMeetingProviderState();
  const meetingDispatch = useMeetingProviderDispatch();
  const history = useHistory();

  const [state, dispatch] = useReducer(reducer, {});


  const toggleVideo = (): void => {
    meetingDispatch({
      type: !isSharingLocalVideo ? StartLocalVideo: StopLocalVideo,
    });
    return history.push('/call');
  };

  useEffect(() => {
    if (!activeMeeting) {
      history.push("/");
    }
  }, [activeMeeting]);

  useEffect(() => {
    MeetingManager.setupSubscribeToAttendeeIdPresenceHandler(meetingDispatch, ''); // todo
  }, []);

  const videoTileDidUpdate = (tileState: any) => {
    meetingDispatch({ type: TileUpdated, payload: tileState });
  };

  const videoTileWasRemoved = (tileId: number) => {
    dispatch({ type: TileDeleted, payload: tileId });
  };

  const nameplateDiv = () =>
    document.getElementById('share-content-view-nameplate') as HTMLDivElement;

  const videoObservers = { videoTileDidUpdate, videoTileWasRemoved };

  useEffect(() => {
    if (activeMeeting) {
      MeetingManager.addAudioVideoObserver(videoObservers);

      return () => {
        console.log('deleted!')
      };
    }
  }, [activeMeeting]);

  return (
    <HomePresenter
      title={authenticate}
      participants={participants}
      toggleVideo={toggleVideo}
    />
  );
};

export default HomeContainer;