import React from 'react';
import './index.scss';
import Video from "../../../components/Video/VideoContainer";

interface IProps {
  participants: any
  closeCall: any
  toggleAudio: any
  toggleVideo: any
  shareScreen: any
}

const CallPresenter: React.FC<IProps> = ({
  participants,
  closeCall,
  toggleAudio,
  toggleVideo,
  shareScreen
}) => {
  return (
    <div className="call-view">
      <div className="call-view__tracks">
        <Video />
      </div>

      <div className="call-view__controls-container">
        <div className="call-view__controls">
          <div id="btn--end-call" className="call-view__controls__icon-btn important">
            <i className="material-icons-round" style={{color: '#FAFAFA'}}>call_end</i>
          </div>
          <div id="btn--toggle-mic" className="call-view__controls__icon-btn">
            <i className="material-icons-round" style={{color: '#FF3346'}}>mic_off</i>
          </div>
          <div id="btn--toggle-cam" className="call-view__controls__icon-btn">
            <i className="material-icons-round" style={{color: '#FF3346'}}>videocam_off</i>
          </div>
          <div id="btn--toggle-screen-sharing" className="call-view__controls__icon-btn">
            <i className="material-icons-round" style={{color: '#27A4FD'}}>screen_share</i>
          </div>
          <div id="btn--settings" className="call-view__controls__icon-btn">
            <i className="material-icons-round" style={{color: '#27A4FD'}}>settings</i>
          </div>
        </div>
      </div>
    </div>
  )
};

export default CallPresenter;