import React, {useEffect} from 'react';
import CallPresenter from "../CallPresenter";
import MeetingProvider, {useMeetingProviderDispatch, useMeetingProviderState} from "../../../context/MeetingProvider";
import { ActionType } from "../../../context/MeetingProvider/types";
import MeetingManager from "../../../lib/MeetingManager/MeetingManager";

const {
  JoinMeeting,
  StartScreenShareView,
  StartLocalVideo,
  StopLocalVideo
} = ActionType;

const CallContainer = () => {
  const { activeMeeting, isSharingLocalVideo } = useMeetingProviderState();
  const dispatch = useMeetingProviderDispatch();


  return (
    <CallPresenter
      participants={{}}
      closeCall={{}}
      toggleAudio={{}}
      toggleVideo={{}}
      shareScreen={{}}
    />
  )
};

export default CallContainer;