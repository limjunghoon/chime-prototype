import React, {useEffect} from "react";
import EnterPresenter from "../EnterPresenter";
import {useAuthProviderState} from "../../../context/AuthProvider";
import {useMeetingProviderDispatch, useMeetingProviderState} from "../../../context/MeetingProvider";
import { ActionType } from "../../../context/MeetingProvider/types";
import {useHistory} from "react-router-dom";

const {
  EnterMeeting
} = ActionType;

const EnterContainer: React.FC = () => {
  const { authenticate } = useAuthProviderState();
  const { activeMeeting } = useMeetingProviderState();
  const dispatch = useMeetingProviderDispatch();
  const history = useHistory();

  const enterMeeting = (): void => {
    dispatch({
      type: EnterMeeting
    });
  };

  useEffect(() => {
    if (activeMeeting) {
      history.push("/home")
    }
  }, [activeMeeting]);

  return (
    <EnterPresenter
      title={authenticate}
      enterMeeting={enterMeeting}
    />
  );
};

export default EnterContainer;