import React from "react";
import Button from "@material-ui/core/Button";

import './index.css';

interface IProps {
  title: string;
  enterMeeting: any;
}

const EnterPresenter: React.FC<IProps> = ({ title, enterMeeting }) => {
  return (
    <section className="mainApp">

      {/* container */}
      <div className="Aligner">
        <div className="Aligner-item">
          <Button onClick={enterMeeting} variant="outlined" color="primary">
            참가하기({title})
          </Button>
        </div>
      </div>
    </section>
  );
};

export default EnterPresenter;