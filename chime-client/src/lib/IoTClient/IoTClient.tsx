import AWS from 'aws-sdk';
import { device as AwsIotClient } from 'aws-iot-device-sdk';

const AWS_CONFIGS = {
  identityPoolId: 'ap-northeast-2:483c9b16-26c2-40fa-961e-a709ee112703',
  host: '',
  region: 'ap-northeast-2',
};

export default class IoTClient {
  public iotClient: AwsIotClient | undefined;
  private static instance: IoTClient;

  public static getInstance(): IoTClient {
    if (!IoTClient.instance) {
      IoTClient.instance = new IoTClient();
    }
    return IoTClient.instance;
  }

  private constructor() {
    this.setupAwsIotClient();
    this.setupCognitoCredentials();
  }

  private setupAwsIotClient() {
    this.iotClient = new AwsIotClient({
      region: AWS_CONFIGS.region,
      host: AWS_CONFIGS.host,
      protocol: 'wss',
      accessKeyId: "",
      secretKey: "",
      sessionToken: ""
    });
  }

  private setupCognitoCredentials() {
    AWS.config.region = AWS_CONFIGS.region;
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: AWS_CONFIGS.identityPoolId,
    });

    const cognitoIdentity = new AWS.CognitoIdentity();
    const credentials = AWS.config.credentials as AWS.CognitoIdentityCredentials;
    const client = this.iotClient;
    credentials.get(function(err) {
      if (!err) {
        const params = {
          IdentityId: credentials.identityId,
        };

        cognitoIdentity.getCredentialsForIdentity(params, function(error, data) {
          if (!error) {
            // Update AWS Credentials, iotClient will use these during next reconnect
            if (client) {
              client.updateWebSocketCredentials(
                (data.Credentials as any).AccessKeyId,
                (data.Credentials as any).SecretKey,
                (data.Credentials as any).SessionToken,
                (data.Credentials as any).Expiration
              );
            }
            console.log('No error retrieving credentials');
          } else {
            console.log('Error retrieving credentials: ' + error);
          }
        });
      } else {
        console.error('Error retrieving Cognito Identity: ' + err);
      }
    });
  }

  public publish(topic: string, message: Buffer | string) {
    (this.iotClient as any).publish(topic, message, undefined, this.handlePublishError);
  }

  private handlePublishError(err: any) {
    if (err) {
      console.error("Error publishing message to AWS IoT: " + err);
    }
  }
}