import {
  AudioVideoFacade,
  AudioVideoObserver,
  ConsoleLogger,
  DefaultDeviceController,
  DefaultMeetingSession,
  LogLevel,
  MeetingSessionConfiguration,
  ScreenShareViewFacade,
  ScreenObserver,
} from 'amazon-chime-sdk-js';
import React from "react";
import { ActionType } from "../../context/MeetingProvider/types";
import { BASE_URL } from "../../config";

const {
  EnterMeeting,
  EnterParticipant
} = ActionType;

class MeetingManager {
  private meetingSession: DefaultMeetingSession | any;
  private audioVideo: AudioVideoFacade | any;
  private screenShareView: ScreenShareViewFacade | any;
  private title: string | any;
  private roster: [] | any;
  private initSub: boolean | any;

  async initializeMeetingSession(configuration: MeetingSessionConfiguration): Promise<any> {
    console.log('================== init ====================');
    const logger = new ConsoleLogger('DEV-SDK', LogLevel.OFF);
    const deviceController = new DefaultDeviceController(logger);
    configuration.enableWebAudio = false;
    this.meetingSession = new DefaultMeetingSession(configuration, logger, deviceController);
    this.audioVideo = this.meetingSession.audioVideo;
    // TODO: Update ScreenShareView to use new introduced content-based screen sharing.
    this.screenShareView = this.meetingSession.screenShareView;
    await this.setupAudioDevices();
    // this.setupSubscribeToAttendeeIdPresenceHandler();
  }

  async setupSubscribeToAttendeeIdPresenceHandler(dispatch: any, interview_idx: any) {
    if (this.initSub) return;
    this.initSub = true;

    const handler = async (attendeeId: string, present: boolean): Promise<any>  => {
      if (!present) {
        await fetch(`${BASE_URL}api/meeting/leave`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            interview_idx,
            attendeeId
          })
        });

        dispatch({
          type: EnterParticipant,
          payload: {
            interview_idx
          }
        });
        return;
      } else {
        dispatch({
          type: EnterParticipant,
          payload: {
            interview_idx
          }
        });
      }

      this.audioVideo.realtimeSubscribeToVolumeIndicator(
        attendeeId,
        async (
          attendeeId: string,
          volume: number | null,
          muted: boolean | null,
          signalStrength: number | null
        ) => {
          // todo 오디오 들어올때
          // if (!this.roster[attendeeId]) {
          //   this.roster[attendeeId] = { name: '' };
          // }
          // if (volume !== null) {
          //   this.roster[attendeeId].volume = Math.round(volume * 100);
          // }
          // if (muted !== null) {
          //   this.roster[attendeeId].muted = muted;
          // }
          // if (signalStrength !== null) {
          //   this.roster[attendeeId].signalStrength = Math.round(signalStrength * 100);
          // }
          // if (!this.roster[attendeeId].name) {
          //   const baseAttendeeId = new DefaultModality(attendeeId).base();
          //   const response = await fetch(`${DemoMeetingApp.BASE_URL}attendee?title=${encodeURIComponent(this.meeting)}&attendee=${encodeURIComponent(baseAttendeeId)}`);
          //   const json = await response.json();
          //   let name = json.AttendeeInfo.Name;
          //   if (baseAttendeeId !== attendeeId) {
          //     name += " «Content»";
          //     const selfAttendeeId = this.meetingSession.configuration.credentials.attendeeId;
          //     //If someone else share content, stop the current content share
          //     if (!this.allowMaxContentShare() && selfAttendeeId !== baseAttendeeId && this.isButtonOn('button-content-share')) {
          //       this.contentShareStop();
          //     }
          //   }
          //   this.roster[attendeeId].name = name ? name : '';
          // }
          // this.updateRoster();
        }
      );
    };

    this.audioVideo.realtimeSubscribeToAttendeeIdPresence(handler);
  }

  async setupAudioDevices(): Promise<void> {
    const audioOutput = await this.audioVideo.listAudioOutputDevices();
    const defaultOutput = audioOutput[0] && audioOutput[0].deviceId;
    await this.audioVideo.chooseAudioOutputDevice(defaultOutput);

    const audioInput = await this.audioVideo.listAudioInputDevices();
    const defaultInput = audioInput[0] && audioInput[0].deviceId;
    await this.audioVideo.chooseAudioInputDevice(defaultInput);
  }

  registerScreenShareObservers(observer: ScreenObserver): void {
    if (!this.screenShareView) {
      console.log('ScreenView not initialize. Cannot add observer');
      return;
    }
    this.screenShareView.registerObserver(observer);
  }

  addAudioVideoObserver(observer: AudioVideoObserver): void {
    if (!this.audioVideo) {
      console.error('AudioVideo not initialized. Cannot add observer');
      return;
    }
    this.audioVideo.addObserver(observer);
  }

  removeMediaObserver(observer: AudioVideoObserver): void {
    if (!this.audioVideo) {
      console.error('AudioVideo not initialized. Cannot remove observer');
      return;
    }

    this.audioVideo.removeObserver(observer);
  }

  removeScreenShareObserver(observer: ScreenObserver): void {
    if (!this.screenShareView) {
      console.error('ScreenView not initialized. Cannot remove observer');
      return;
    }

    this.screenShareView.unregisterObserver(observer);
  }

  bindVideoTile(id: number, videoEl: HTMLVideoElement): void {
    this.audioVideo.bindVideoElement(id, videoEl);
  }

  async startLocalVideo(): Promise<void> {
    try {
      const videoInput = await this.audioVideo.listVideoInputDevices();
      const defaultVideo = videoInput[0];
      await this.audioVideo.chooseVideoInputDevice(defaultVideo);
      this.audioVideo.startLocalVideoTile();
    } catch(e) {
      console.log(e)
    }
  }

  stopLocalVideo(): void {
    this.audioVideo.stopLocalVideoTile();
  }

  async startViewingScreenShare(screenViewElement: HTMLDivElement): Promise<void> {
    console.log('----------------------------------------------------------')
    console.log(this.screenShareView)
    this.screenShareView.start(screenViewElement).catch((error: any) => console.error(error));
  }

  stopViewingScreenShare(): void {
    this.screenShareView.stop().catch((error: any) => {
      console.error(error);
    });
  }

  async joinMeeting(meetingId: string, name: string): Promise<any> {
    try {
      const res = await fetch(`${BASE_URL}api/meeting/create`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          meetingId: encodeURIComponent('test123'),
          name: encodeURIComponent(name)
        })
      });

      const { payload: { meetingInfo }} = await res.json();

      this.title = meetingId;

      await this.initializeMeetingSession(
        new MeetingSessionConfiguration(...meetingInfo)
      );
      await this.meetingSession.screenShareView.open();
      this.audioVideo.start();
    } catch(e) {
      console.log(e);
    }
  }

  async enterMeeting(): Promise<any> {
    try {
      const { ok, payload, error } = await (await fetch(`${BASE_URL}api/meeting/enter`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        }
      })).json();

      if (!ok) return console.log(error), alert(error);

      const enterResponse = payload.enterResponse;

      await this.initializeMeetingSession(
        new MeetingSessionConfiguration(...enterResponse)
      );
      await this.meetingSession.screenShareView.open();
      this.audioVideo.start();

    } catch(e) {
      console.log(e);
      return;
    }

    return true;
  }

  async endMeeting(): Promise<any> {
    await fetch(`${BASE_URL}end?title=${encodeURIComponent(this.title)}`, {
      method: 'POST',
    });
    this.leaveMeeting();
  }

  leaveMeeting(): void {
    this.stopViewingScreenShare();
    this.meetingSession.screenShareView.close();
    this.audioVideo.stop();
  }

  async getAttendees(meetIdx: string): Promise<any> {
    let result = [];
    try {
      const { payload: { attendees }} = await (await fetch(`${BASE_URL}api/meeting/attendees?meetIdx=${meetIdx}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
      })).json();

      result = attendees;
    } catch(e) {
      console.log(e);
    }

    return result;
  }

  async getAttendee(attendeeId: string): Promise<string> {
    const response = await fetch(
      `${BASE_URL}attendee?title=${encodeURIComponent(this.title)}&attendee=${encodeURIComponent(
        attendeeId
      )}`
    );
    const json = await response.json();
    return json.AttendeeInfo.Name;
  }

  bindAudioElement(ref: HTMLAudioElement) {
    this.audioVideo.bindAudioElement(ref);
  }

  unbindAudioElement(): void {
    this.audioVideo.unbindAudioElement();
  }
}

export default new MeetingManager();
