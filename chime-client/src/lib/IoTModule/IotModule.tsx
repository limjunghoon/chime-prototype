import { device as AwsIotClient } from 'aws-iot-device-sdk';

const IotModule = (awsIotClient: AwsIotClient) => {
  let iotClient: AwsIotClient;

  window.onConnectHandler = function() {
    console.log("IoT client - connect to IoT");
  };

  window.onReconnectHandler = function() {
    console.log("IoT client - re-connect to IoT");
  };

  window.onMessageHandler = function(topic: any, payload: any) {
    console.log('Receiving message from topic - ' + topic + ': ' + payload.toString());
  };


  iotClient = awsIotClient;
  iotClient.on("connect", window.onConnectHandler);
  iotClient.subscribe('iot/meeting/#');

  iotClient.on("reconnect", window.onReconnectHandler);
  iotClient.on("message", window.onMessageHandler);
};

export default IotModule;