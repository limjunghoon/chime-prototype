import {
  ConsoleLogger,
  DefaultDeviceController,
  DefaultMeetingSession,
  LogLevel,
  MeetingSessionConfiguration
} from 'amazon-chime-sdk-js';

// You need responses from server-side Chime API. See below for details.
async function requestChime() {
  return await ((await fetch('http://localhost:3001/api/meeting/create')).json());
}

async function createMeetingSession() {
  const response = await requestChime();
  if (!response.ok) return;

  const { payload: { meetingInfo }} = response;
  if (!meetingInfo || !meetingInfo.length) return;

  const logger = new ConsoleLogger('SDK', LogLevel.INFO);
  const deviceController = new DefaultDeviceController(logger);
  const meetingSessionConfiguration = new MeetingSessionConfiguration(...meetingInfo);
  meetingSessionConfiguration.enableWebAudio = false;
  meetingSessionConfiguration.enableUnifiedPlanForChromiumBasedBrowsers = true;

  return new DefaultMeetingSession(meetingSessionConfiguration, logger, deviceController);
}

async function initMeetingSession() {
  const meetingSession = await createMeetingSession();
  if (!meetingSession) return;

  const audioVideo = meetingSession.audioVideo;
  const screenShareView = meetingSession.screenShareView;

  // setup audio device
  const audioOutput = await audioVideo.listAudioOutputDevices();
  const defaultOutput = audioOutput[0] && audioOutput[0].deviceId;
  await audioVideo.chooseAudioOutputDevice(defaultOutput);

  const audioInput = await audioVideo.listAudioInputDevices();
  const defaultInput = audioInput[0] && audioInput[0].deviceId;
  await audioVideo.chooseAudioInputDevice(defaultInput);
}

export default createMeetingSession;