import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import AuthProvider from "./context/AuthProvider";
import MeetingProvider from "./context/MeetingProvider";

export type DeviceMessage = {
  [key: string]: any;
};

export interface MessageHandler {
  (message: DeviceMessage): void;
}


export interface DeviceEnvironment {
  init(listesner: MessageHandler): any;
  sendMessage(message: DeviceMessage): void;
}


declare global {
  interface Window {
    [key: string]: any;
    // controllerEnvironment?: Promise<DeviceEnvironment>;
    deviceEnvironment?: Promise<DeviceEnvironment>;
  }
}

ReactDOM.render(
  <AuthProvider>
    <MeetingProvider>
      <App />
    </MeetingProvider>
  </AuthProvider>,
  document.getElementById('root')
);
