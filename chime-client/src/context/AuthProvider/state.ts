export interface AuthState {
  authenticate: Authenticate;
  info: any;
}

export enum Authenticate {
  Manager = 'MANAGER',
  Guest = 'GUEST',
  Unknown = 'UNKNOWN'
}

const state: AuthState = {
  authenticate: Authenticate.Guest,
  info: {}
};

export default state;