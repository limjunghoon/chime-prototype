import { AuthState, Authenticate } from './state';

enum ActionType {
  ManagerLogin = 'MANAGER_LOGIN',
  MemberLogin = 'MEMBER_LOGIN',
  GuestLogin = 'GUEST_LOGIN',
  AccessDenied = 'ACCESS_DENIED'
}

interface AuthAction {
  type: ActionType;
  payload?: any;
}

const {
  ManagerLogin,
  GuestLogin,
  AccessDenied
} = ActionType;

const {
  Manager,
  Guest,
  Unknown
} = Authenticate;

const reducer = (state: AuthState, action: AuthAction): AuthState => {
  const { type, payload } = action;

  switch (type) {
    case ManagerLogin:
      return {
        ...state,
        authenticate: Manager,
        info: payload
      };
    case GuestLogin:
      return {
        ...state,
        authenticate: Guest,
        info: payload
      };
    case AccessDenied:
      return {
        ...state,
        authenticate: Unknown,
      };
    default:
      return state;
  }
}

export default reducer;
export { ActionType };