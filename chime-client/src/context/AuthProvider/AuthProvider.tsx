import React, {
  createContext,
  useReducer,
  useContext
} from 'react';
import initialState from './state';
import reducer from './reducer';

const AuthProviderStateContext = createContext({});
const AuthProviderDispatchContext = createContext({});

const AuthProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AuthProviderStateContext.Provider value={state}>
      <AuthProviderDispatchContext.Provider value={dispatch}>
        { children }
      </AuthProviderDispatchContext.Provider>
    </AuthProviderStateContext.Provider>
  )
};

const useAuthProviderState = (): any => {
  const context = useContext(AuthProviderStateContext);
  if (context === undefined) throw new Error('useAuthProviderState must be used within a Provider');

  return context;
};

const useAuthProviderDispatch = (): any => {
  const context = useContext(AuthProviderDispatchContext);
  if (context === undefined) throw new Error('useAuthProviderDispatch must be used within a Provider');

  return context;
};

export default AuthProvider;
export {
  useAuthProviderState,
  useAuthProviderDispatch
}