import { State } from "./types";

const initialState: State = {
  activeMeeting: false,
  isSharingLocalVideo: false,
  isViewingSharedScreen: false,
  participants: [],
  tileState: {}
};

export default initialState;