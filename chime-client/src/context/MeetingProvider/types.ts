export interface Participant {
  attIdx: string;
  meetIdx?: number;
  memIdx?: string;
  attendeeInfo?: any;
  reg_dt?: any;
  edit_dt?: any;
  attendeeId?: any;
  profile?: any;
}

export interface State {
  activeMeeting: boolean;
  isSharingLocalVideo: boolean;
  isViewingSharedScreen: boolean;
  participants: Participant[];
  tileState: any
}

export enum ActionType {
  JoinMeeting = 'JOIN_MEETING',
  EnterMeeting = 'ENTER_MEETING',
  EnterParticipant = 'ENTER_PARTICIPANT',
  StartLocalVideo = 'START_LOCAL_VIDEO',
  StopLocalVideo = 'STOP_LOCAL_VIDEO',
  EndMeeting = 'END_MEETING',
  LeaveMeeting = 'LEAVE_MEETING',
  StartScreenShareView = 'START_SCREEN_SHARE_VIEW',
  StopScreenShareView = 'STOP_SCREEN_SHARE_VIEW',
  TileUpdated = 'TILE_UPDATED',
  TileDeleted = 'TILE_DELETED'
}

export interface Action {
  type: ActionType;
  payload?: any;
}
