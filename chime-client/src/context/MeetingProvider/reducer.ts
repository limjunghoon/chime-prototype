import { State, Action, ActionType } from "./types";
import initialState from "./store";

const {
  JoinMeeting,
  EnterMeeting,
  EnterParticipant,
  StartLocalVideo,
  StopLocalVideo,
  EndMeeting,
  LeaveMeeting,
  StartScreenShareView,
  StopScreenShareView,
  TileUpdated,
  TileDeleted
} = ActionType;

const reducer = (state: State, action: Action) => {
  const { type, payload } = action;
  switch (type) {
    case JoinMeeting:
      return {
        ...state,
        activeMeeting: true,
      };
    case EnterMeeting:
      return {
        ...state,
        activeMeeting: true
      };
    case EnterParticipant:
      return {
        ...state,
        participants: payload
      };
    case StartLocalVideo:
      return {
        ...state,
        isSharingLocalVideo: true,
      };
    case StopLocalVideo:
      return {
        ...state,
        isSharingLocalVideo: false,
      };
    case EndMeeting:
      return {
        ...initialState,
      };
    case LeaveMeeting:
      return {
        ...initialState,
      };
    case StartScreenShareView:
      return {
        ...state,
        isViewingSharedScreen: true,
      };
    case StopScreenShareView:
      return {
        ...state,
        isViewingSharedScreen: false,
      };
    case TileUpdated:
      return {
        ...state,
        tileState: payload
      }
    case TileDeleted:
      return {
        ...state,
        tileState: payload
      }
    default:
      return state;
  }
}

export default reducer;
