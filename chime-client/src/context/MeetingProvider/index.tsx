import React, {
  useEffect,
  useReducer,
  useRef,
  createContext,
  useContext,
  useCallback,
} from 'react';
import { useHistory } from 'react-router-dom';
import MeetingManager from "../../lib/MeetingManager";
// import routes from '../../../routes';
import reducer from './reducer';
import initialState from './store';
import { DeviceMessage, MessageHandler } from "../../index";
import { screenViewDiv } from "../../components/Video/VideoContainer";
import {DeviceEnvironment} from "../../index";
import { State, Action, ActionType } from "./types";

const {
  JoinMeeting,
  EnterMeeting,
  EnterParticipant,
  StartLocalVideo,
  StopLocalVideo,
  EndMeeting,
  LeaveMeeting,
  StartScreenShareView,
  StopScreenShareView,
  TileUpdated,
  TileDeleted
} = ActionType;

const MeetingProviderState = createContext({});
const MeetingProviderDispatch = createContext({});

const sendMessage = async (msg: DeviceMessage): Promise<void> => {
  if (!window.deviceEnvironment) return;
  console.log(`Sending message to controller ${msg.type}`);

  const env = await window.deviceEnvironment;
  env.sendMessage(msg);
};

const MeetingProvider: React.FC = ({ children }) => {
  let history = useHistory();
  const [state, dispatch] = useReducer(reducer, initialState);
  const isInitialized = useRef(false);

  const wrappedDispatch = useCallback(msg => {
    dispatchHandler(msg);
  }, []);

  const dispatchHandler: MessageHandler = async ({ type, payload }) => {
    console.log(`RoomProvider::messageHandler - Message received with type: ${type}`);
    try {
      switch (type) {
        case JoinMeeting:
          const { meetingId, name } = payload;
          if (!meetingId || !name) return;

          await MeetingManager.joinMeeting(meetingId, name);
          dispatch({ type: JoinMeeting });
          // history.push(routes.MEETING);
          break;
        case EnterMeeting:
          if (await MeetingManager.enterMeeting()) {
            dispatch({
              type: EnterMeeting
            });
          }

          // history.push(routes.MEETING);
          break;
        case EnterParticipant:
          const { meetIdx: meetIdx2 } = payload;
          const attendees = await MeetingManager.getAttendees(meetIdx2);

          if (state.participants.length !== attendees.length) {
            dispatch({
              type: EnterParticipant,
              payload: attendees
            });
          }
          break;
        case StartLocalVideo:
          MeetingManager.startLocalVideo();
          dispatch({ type: StartLocalVideo });
          break;
        case StopLocalVideo:
          MeetingManager.stopLocalVideo();
          dispatch({ type: StopLocalVideo });
          break;
        case LeaveMeeting:
          await MeetingManager.leaveMeeting();
          // history.push(routes.ROOT);
          dispatch({ type: LeaveMeeting });
          break;
        case EndMeeting:
          await MeetingManager.endMeeting();
          // history.push(routes.ROOT);
          dispatch({ type: EndMeeting });
          break;
        case StartScreenShareView:
          MeetingManager.startViewingScreenShare(screenViewDiv());
          dispatch({ type: StartScreenShareView });
          break;
        case StopScreenShareView:
          MeetingManager.stopViewingScreenShare();
          dispatch({ type: StopScreenShareView });
          break;
        case TileUpdated:
          const { tileId, ...rest } = payload;
          state.tileState[tileId] = {
            ...rest
          }
          dispatch({
            type: StopScreenShareView,
            payload: {
              ...state.tileState
            }
          });
          break;
        case TileDeleted:
          // @ts-ignore
          const { [payload]: omit, ...rests } = state;
          dispatch({
            type: TileDeleted,
            payload: {
              ...rests
            }
          });
          break;
        default:
          console.log(`Unhandled incoming message: ${type}`);
          break;
      }
    } catch (e) {
      alert(e);
    }
  };

  return (
    <MeetingProviderState.Provider value={state}>
      <MeetingProviderDispatch.Provider value={wrappedDispatch}>
        {children}
      </MeetingProviderDispatch.Provider>
    </MeetingProviderState.Provider>
  );
};

export function useMeetingProviderDispatch(): any {
  const context = useContext(MeetingProviderDispatch);
  if (context === undefined) {
    throw new Error('useRoomProviderDispatch must be used within a Provider');
  }
  return context;
}

export function useMeetingProviderState(): any {
  const context = useContext(MeetingProviderState);
  if (context === undefined) {
    throw new Error('useRoomProviderState must be used within a Provider');
  }
  return context;
}

export default MeetingProvider;
