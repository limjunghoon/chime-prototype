const BASE_URL = [
  window.location.protocol,
  '//',
  // window.location.host,
  `${window.location.hostname}:3000`,
  window.location.pathname.replace(/\/*$/, '/'),
].join('');


export {
  BASE_URL
}