import React from 'react';
import './index.css';

const NotFound: React.FC = () => {
  return (
    <div id="main">
      <div className="fof">
        <h1>Access Denied</h1>
      </div>
    </div>
  );
};

export default NotFound;