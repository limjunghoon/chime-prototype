import React, {useState, useContext, useEffect} from 'react';
import { useHistory } from 'react-router-dom';
import AppPresenter from "./AppPresenter";
import AuthProvider, {
  useAuthProviderState,
  useAuthProviderDispatch
} from "../../context/AuthProvider/AuthProvider";
import { ActionType } from "../../context/AuthProvider/reducer";
import { BASE_URL } from "../../config";
import queryString from 'query-string';

const {
  ManagerLogin,
  GuestLogin,
  AccessDenied
} = ActionType;

const AppContainer: React.FC = ({ children }) => {
  const { authenticate } = useAuthProviderState();
  const authDispatch = useAuthProviderDispatch();

  useEffect(() => {
    const verifySession = async () => {
      const params = queryString.parse(window.location.search);

      try {
        let {
          ok,
          error,
          payload
        } = await (await fetch(`${BASE_URL}api/auth/verify-session?ssid=${params.ssid ? params.ssid : ''}&interview_idx=${params.interview_idx ? params.interview_idx : ''}`)).json();

        if (!ok) {
          authDispatch({
            type: AccessDenied
          });
        } else {
          const { mem_gb } = payload;
          authDispatch({
            type: (mem_gb === 'c' ? ManagerLogin : GuestLogin),
            payload
          });
        }
      } catch(e) {
        console.log(e);
      }
    };

    verifySession();
  }, []);

  return (
    <AppPresenter authenticate={authenticate} />
  )
};

export default AppContainer;