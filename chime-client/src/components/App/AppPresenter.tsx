import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Authenticate } from "../../context/AuthProvider/state";
import NotFound from "../NotFound";
import Call from "../../routes/Call/CallContainer";
import Home from "../../routes/Home";
import Enter from "../../routes/Enter";

const {
  Unknown
} = Authenticate;

interface IProps {
  authenticate: Authenticate;
}

const PublicRoutes: React.FC = () => (
  <Switch>
    <Route path={'/'} exact component={NotFound} />
  </Switch>
);

const PrivateRoutes: React.FC = () => (
  <Switch>
    <Route path={'/'} exact component={Enter} />
    <Route path={'/home'} exact component={Home} />
    <Route path={'/call'} exact component={Call} />
    <Route path={'/error'} exact component={NotFound} />
  </Switch>
);

const AppPresenter: React.FC<IProps> = ({ authenticate }) => {

  return (
    <Router>
      {authenticate === Unknown
        ? <PublicRoutes/>
        : <PrivateRoutes/>
      }
    </Router>
  );
};

export default AppPresenter;