import React, { useReducer, useEffect } from 'react';

import MeetingManager from "../../../lib/MeetingManager";

import { ScreenMessageDetail } from 'amazon-chime-sdk-js';
import { DeviceMessage } from "../../../index";
import { ActionType } from "../../../context/MeetingProvider/types";
import { useMeetingProviderState, useMeetingProviderDispatch } from "../../../context/MeetingProvider";
import reducer from '../VideoContainer/reducer';
import VideoPresenter from "../VideoPresenter";
import {useAuthProviderState} from "../../../context/AuthProvider";
import {useHistory} from "react-router-dom";

const {
  JoinMeeting,
  EnterMeeting,
  StartLocalVideo,
  StopLocalVideo,
  StartScreenShareView,
  StopScreenShareView
} = ActionType;

// TODO: Make as component.
export const screenViewDiv = () => document.getElementById('shared-content-view') as HTMLDivElement;

const VideoContainer: React.FC = ({

}) => {
  const { tileState } = useMeetingProviderState();

  return (
    <VideoPresenter
      state={tileState}
      bindVideoTile={tileId => {
        return ((videoRef: any) => {
          MeetingManager.bindVideoTile(parseInt(tileId), videoRef);
        })
      }}
    />
  );
}

export default VideoContainer;
