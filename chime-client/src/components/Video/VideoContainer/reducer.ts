const reducer = (state: any, { type, payload }: any) => {
  switch (type) {
    case 'TILE_UPDATED': {
      const { tileId, ...rest } = payload;
      return {
        ...state,
        [tileId]: {
          ...rest,
        },
      };
    }
    case 'TILE_DELETED': {
      const { [payload]: omit, ...rest } = state;
      return {
        ...rest,
      };
    }
    default: {
      return state;
    }
  }
}

export default reducer;