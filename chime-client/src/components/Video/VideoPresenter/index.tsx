import React, {useEffect, useRef} from 'react';
import Button from "@material-ui/core/Button";
import MeetingManager from "../../../lib/MeetingManager/MeetingManager";
import VideoTile from "./VideoTile";
import './index.scss';

interface IProps {
  state: any
  bindVideoTile: (titleId: any) => any
}

const VideoPresenter: React.FC<IProps> = ({
  state,
  bindVideoTile,
}) => {
  return (
    <>
      {Object.keys(state).map(tileId => (
        <div className="remote-track--container">
          <div
            className={`VideoGrid`}>
            <VideoTile
              key={tileId}
              nameplate="Attendee ID"
              isLocal={state[tileId].localTile}
              bindVideoTile={(videoRef: any) => bindVideoTile(tileId)(videoRef)}
            />
          </div>
        </div>
      ))};
    </>
  );
};

export default VideoPresenter;