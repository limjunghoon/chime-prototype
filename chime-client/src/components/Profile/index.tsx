import React from 'react';
import {Participant} from "../../context/MeetingProvider/types";

interface IProps extends Participant {
  isActive: boolean;
  changeActive: any;
}

const Profile: React.FC<IProps> = ({
  attIdx,
  memIdx,
  attendeeId,
  profile,
  isActive,
  changeActive
}) => {
  return (
    <div className={`chatButton ${isActive ? 'active' : ''}`} onClick={changeActive}>
      <div className="chatInfo">
        <div className="image my-image">

        </div>

        <p className="name">
          {profile ? profile.name : `참가자_${attIdx}`}
        </p>

        <p className="message">
          {profile ? profile.phone : '010-****-****'}
        </p>
      </div>

      <div className="status onTop">
      </div>
    </div>
  );
};

export default Profile;