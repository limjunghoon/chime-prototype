import Router from 'koa-router';
import MakeSession from './MakeSession';

const test = new Router();

test.post('/create', MakeSession);
export default test;