import Router from 'koa-router';
import VerifySession from './VerifySession';

const auth = new Router();
auth.get('/verify-session', VerifySession);

export default auth;
