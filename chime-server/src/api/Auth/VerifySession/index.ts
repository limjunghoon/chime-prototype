import sessionStore from "../../../lib/sessionStore";
import { response } from "../../../lib/util";
import { TEST_SESSION } from "../../../config";

const QueryGetSession: any = async ssid => {
  return TEST_SESSION
    ? TEST_SESSION
    : await sessionStore.get(ssid);
};

const VerifySession = async ctx => {
  let { request: { query: {
    ssid = ctx.request.query.ssid,
    interview_idx = ctx.request.query.interview_idx
  }}} = ctx;

  if (!ssid || !interview_idx) return response(ctx, {
    error: '올바르지 않은 접근입니다.'
  });

  const session = await QueryGetSession(ssid);
  if (!session || !(session as any).mem_idx) return response(ctx, {
    error: '사림인 로그인 후 이용하실 수 있습니다.'
  });

  ctx.cookies.set('ssid', ssid);
  ctx.cookies.set('interview_idx', interview_idx); // test

  return response(ctx, {
    ok: true,
    payload: session
  });
};

export default VerifySession;
export {
  QueryGetSession
}