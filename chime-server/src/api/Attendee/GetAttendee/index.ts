import { response } from "../../../lib/util";

import Attendee from "../../../entities/Attendee";

interface QueryGetAttendeeParams {
  interview_idx: number;
  mem_idx: number;
  hr_manager_info?: string;
}

const QueryGetAttendee = async ({
  interview_idx,
  mem_idx,
  hr_manager_info = 'chime_test'
}: QueryGetAttendeeParams) => {
  let meeting;
  try {
    meeting = await Attendee.findOne({
      interview_idx,
      mem_idx,
      hr_manager_info
    });
  } catch(e) {
    console.log(e);
  }
  return meeting;
};


const GetAttendee = async (ctx) => {

};

export default GetAttendee;
export {
  QueryGetAttendee
}