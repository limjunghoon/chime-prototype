import {response} from "../../../lib/util";
import ExitAttendee from "../ExitAttendee";
import Redis from 'ioredis';
import {CONFIG_SRAMIN_REDIS} from "../../../config";

const redis = new Redis(CONFIG_SRAMIN_REDIS);

interface QueryEnterAttendeeParams {
  interview_idx: number;
  attendee: string
}

const QueryEnterAttendee = async ({
  interview_idx,
  attendee
}: QueryEnterAttendeeParams): Promise<boolean> => {
  try {
    const attendees = await redis.get(interview_idx);
    const parseAttendees = JSON.parse(attendees ? attendees : '[]');

    redis.set(interview_idx, JSON.stringify(parseAttendees.concat(JSON.parse(attendee))));

    return true;
  } catch(e) {
    console.log(e);

    return false;
  }
}


interface IParams {

}

const EnterAttendee = async ({

}: IParams) => {

};

export default EnterAttendee;
export {
  QueryEnterAttendee
}