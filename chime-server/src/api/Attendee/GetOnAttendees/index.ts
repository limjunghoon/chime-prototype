import { response } from "../../../lib/util";
import Attendee from "../../../entities/Attendee";
import Redis from 'ioredis';
import {QueryGetSession} from "../../Auth/VerifySession";
import { QueryGetMeeting } from "../../Meeting/GetMeeting";
import { CONFIG_SRAMIN_REDIS } from "../../../config";

const redis = new Redis(CONFIG_SRAMIN_REDIS);

interface QueryGetOnAttendeesParams {
  interview_idx: number;
}

const QueryGetOnAttendees = async ({
  interview_idx
}: QueryGetOnAttendeesParams) => {
  let attendees;

  try {
    attendees = await redis.get(interview_idx);
    attendees = JSON.parse(attendees);
  } catch(e) {
    console.log(e);
  }

  return attendees;
}

const GetOnAttendees = async (ctx) => {
  const { cookie: { ssid }} = ctx;

  const { interview_idx, mem_idx } = await QueryGetSession(ssid);

  const meeting = await QueryGetMeeting({
    template_key: interview_idx
  });

  if (!meeting) {
    return response(ctx, {
      error: `meeting: ${meeting}`
    });
  }

  let onAttendees = await QueryGetOnAttendees(interview_idx);

  if (mem_idx !== meeting.mcom_idx) {
    onAttendees = await Promise.all(onAttendees.map(async attendee => {
      delete attendee.send_mail_cnt;
      delete attendee.send_sms_cnt;
      delete attendee.user_phone_num;
      delete attendee.user_email;
      delete attendee.last_school_nm;
      delete attendee.memo;

      return attendee;
    }));
  }

  response(ctx, {
    ok: true,
    payload: {
      attendees: onAttendees
    }
  });
};

export default GetOnAttendees;
export {
  QueryGetOnAttendees
}