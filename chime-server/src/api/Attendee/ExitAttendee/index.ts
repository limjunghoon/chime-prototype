import {CONFIG_SRAMIN_REDIS, TEST_SESSION} from "../../../config";
import Redis from 'ioredis';
import {response} from "../../../lib/util";
import {QueryGetSession} from "../../Auth/VerifySession";

const redis = new Redis(CONFIG_SRAMIN_REDIS);

interface QueryExitAttendeeProps {
  interview_idx: number;
  attendeeId: string;
}

const QueryExitAttendee = async ({
  interview_idx,
  attendeeId
}: QueryExitAttendeeProps): Promise<boolean> => {
  try {
    const attendees = await redis.get(interview_idx);
    if (!attendees) return;

    const filtered = JSON.parse(attendees).filter(attend => attend.attendeeId !== attendeeId);

    redis.set(interview_idx, JSON.stringify(filtered));

    return true;
  } catch(e) {
    console.log(e);
    return false;
  }
}


const ExitAttendee = async (ctx) => {
  const { request: { body: {
    interview_idx = (TEST_SESSION ? TEST_SESSION.interview_idx: ctx.request.body.interview_idx),
    attendeeId = (TEST_SESSION ? TEST_SESSION.attendeeId: ctx.request.body.attendeeId),
    ssid = (TEST_SESSION ? TEST_SESSION.ssid: ctx.request.body.ssid),
  }}} = ctx;

  if (!interview_idx || !attendeeId ) {
    return response(ctx, {
      error: `interview_idx: ${interview_idx}, attendeeId: ${attendeeId}`
    });
  }

  const session = await QueryGetSession(ssid);
  if (!session || !session.mem_idx) return response(ctx, {
    error: '사림인 로그인 후 이용 하실 수 있습니다.'
  });

  QueryExitAttendee({
    interview_idx,
    attendeeId
  })
};

export default ExitAttendee;
export {
  QueryExitAttendee
}