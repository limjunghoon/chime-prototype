import Router from 'koa-router';
import GetOnAttendees from './GetOnAttendees';
import ExitAttendee from "./ExitAttendee";

const attendee = new Router();

attendee.get('/get-on-attendees', GetOnAttendees);
attendee.post('/leave', ExitAttendee);

export default attendee;
