import Router from 'koa-router';
import attendee from "./Attendee";
import auth from './Auth';
import meeting from "./Meeting";

const api = new Router();

api.use('/attendee', attendee.routes());
api.use('/auth', auth.routes());
api.use('/meeting', meeting.routes());

export default api;