import Router from 'koa-router';
import CreateMeeting from './CreateMeeting';
import EnterMeeting from "./EnterMeeting";
import GetMeeting from './GetMeeting';
import LeaveMeeting from "./ExitMeeting";
import JoinMeeting from "./JoinMeeting";

const meeting = new Router();

meeting.post('/create', CreateMeeting);
meeting.post('/enter', EnterMeeting);
meeting.post('/leave', LeaveMeeting);
meeting.post('/join', JoinMeeting);
meeting.get('/get', JoinMeeting);

export default meeting;