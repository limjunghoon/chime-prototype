import AWS from "aws-sdk";
import { v4 as uuid } from 'uuid';
import {response} from "../../../lib/util";
import Redis from 'ioredis';
import Attendee from "../../../entities/Attendee";
import { QueryGetMeeting } from "../GetMeeting";
import { CONFIG_SRAMIN_REDIS } from "../../../config";
import { QueryGetSession } from "../../Auth/VerifySession";
import { QueryEnterAttendee } from "../../Attendee/EnterAttendee";
import {stringify} from "querystring";
import { QueryGetAttendee } from "../../Attendee/GetAttendee";
import {TEST_SESSION} from "../../../config";

const redis = new Redis(CONFIG_SRAMIN_REDIS);

interface GetAttendeeProps {
  interview_idx: number;
  mem_idx: number;
  hr_manager_info?: string;
}

const getAttendee = async ({
  interview_idx,
  mem_idx,
  hr_manager_info
}: GetAttendeeProps) => {
  let attendee;

  try {
    attendee = await Attendee.findOne({
      interview_idx,
      mem_idx,
      hr_manager_info
    });
  } catch(e) {
    console.log(e);
  }

  return attendee;
}
const EnterMeeting = async (ctx) => {
  const { request: { body: {
    interview_idx = ctx.request.body.interview_idx,
    ssid = ctx.request.body.ssid
  }}} = ctx;

  if (!interview_idx || !ssid) {
    return response(ctx, {
      error: `interview_idx: ${interview_idx}, ssid: ${ssid}`
    });
  }

  const session = await QueryGetSession(ssid);
  if (!session || !session.mem_idx) return response(ctx, {
    error: '사림인 로그인 후 이용 하실 수 있습니다.'
  });

  const meeting = await QueryGetMeeting({
    contents_type: 'chime_test',
    template_key: interview_idx + ''
  });

  if (!meeting || !meeting.contents_template) {
    console.log(`meeting: ${meeting}`)
    return response(ctx, {
      error: '미팅을 찾을 수 없습니다.'
    });
  }

  const attendee = await QueryGetAttendee({
    interview_idx,
    mem_idx: session.mem_idx,
    hr_manager_info: 'chime_test'
  });

  if (!attendee) {
    return response(ctx, {
      error: '허용된 참여자가 아닙니다.'
    });
  }

  let attendees = await redis.get(interview_idx);
  if (attendees) {
    const parseAttendees = JSON.parse(attendees);
    if (parseAttendees.some(oldAttendee => oldAttendee.interviewee_idx === attendee.interviewee_idx)) {
      return response(ctx, {
        error: `이미 참석 중 입니다. attendees: ${attendees}`
      });
    }
  }

  try {
    const meetingInfos = JSON.parse(meeting.contents_template);
    const meetingResponse = meetingInfos.meetInfo;

    const chime = new AWS.Chime({region: "us-east-1"});
    chime.endpoint = new AWS.Endpoint('https://service.chime.aws.amazon.com/console');

    const attendeeResponse = await chime.createAttendee({
      MeetingId: meetingResponse.Meeting.MeetingId,
      ExternalUserId: uuid()
    }).promise();

    if (
      !(await QueryEnterAttendee({
        interview_idx,
        attendee: JSON.stringify(Object.assign(attendee, {
          attendeeId: attendeeResponse.Attendee.AttendeeId
        }))
      }))
    ) return response(ctx, { error: '미팅 대화방 참여 실패' });

    return response(ctx, {
      ok: true,
      payload: {
        enterResponse: [meetingResponse, attendeeResponse]
      }
    });
  } catch(e) {
    return response(ctx, {
      error: e
    });
  }
};

export default EnterMeeting;
export {
  getAttendee
}