import { response } from "../../../lib/util";

import Meeting from "../../../entities/Meeting";

interface QueryGetMeetingParams {
  template_key: string,
  contents_type?: string
}

const QueryGetMeeting = async ({
  template_key,
  contents_type = 'chime_test'
}: QueryGetMeetingParams) => {
  let meeting;
  try {
    meeting = await Meeting.findOne({
      template_key,
      contents_type
    });
  } catch(e) {
    console.log(e);
  }
  return meeting;
};


const GetMeeting = async (ctx) => {
  const { request: { query: { interview_idx }}} = ctx;
  if (!interview_idx) {
    response(ctx, {
      error: `interview_idx: ${interview_idx}`
    });
  }

  let meeting;

  try {
    meeting = await QueryGetMeeting({
      template_key: interview_idx
    });

    if (!meeting) throw new Error('미팅 모델 조회 실패')
  } catch(e) {
    response(ctx, {
      error: e
    });
  }

  response(ctx, {
    ok: true,
    payload: {
      meeting
    }
  });
};

export default GetMeeting;
export {
  QueryGetMeeting
}