import AWS from "aws-sdk";
import { v4 as uuid } from 'uuid';
import { response } from "../../../lib/util";
import Meeting from "../../../entities/Meeting";
import {QueryGetSession} from "../../Auth/VerifySession";

interface InsertMeetingProps {
  interview_idx: number;
  mcom_idx: number;
  contents_type: string;
  contents_template: string;
}

const insertMeeting = async ({
  interview_idx,
  mcom_idx,
  contents_type,
  contents_template
}: InsertMeetingProps) => {
  let insertedMeeting;

  try {
    const meeting = new Meeting();

    meeting.template_key = interview_idx + '';
    meeting.mcom_idx = mcom_idx;
    meeting.contents_type = contents_type;
    meeting.contents_template = contents_template;

    insertedMeeting = await meeting.save();

  } catch(e) {
    console.log(e);
  }

  return insertedMeeting;
};

const CreateMeeting = async (ctx) => {
  const { request: { body: {
    interview_idx,
    ssid
  }}} = ctx;

  if (!interview_idx || !ssid ) {
    return response(ctx, {
      error: `interview_idx: ${interview_idx}, ssid: ${ssid}`
    });
  }

  const session = await QueryGetSession(ssid);
  if (!session || !(session as any).mem_idx) return response(ctx, {
    error: '사림인 로그인 후 이용 하실 수 있습니다.'
  });

  const {
    mcom_idx,
    mem_gb,
  } = session;

  if (!mcom_idx || mem_gb !== 'c') {
    return response(ctx, {
      error: '기업회원이 아니면 대화방을 개설할 수 없습니다.'
    });
  }

  let meeting;

  try {
    const chime = new AWS.Chime({region: "us-east-1"});
    chime.endpoint = new AWS.Endpoint('https://service.chime.aws.amazon.com/console');

    const meetingResponse = await chime.createMeeting({
      ClientRequestToken: uuid(),
      MediaRegion: 'us-east-1'
    }).promise();

    const contents_type = 'chime_test';
    const contents_template = JSON.stringify({
      meetId: meetingResponse.Meeting.MeetingId,
      meetInfo: meetingResponse
    });

    meeting = await insertMeeting({
      interview_idx,
      mcom_idx,
      contents_type,
      contents_template
    });

  } catch(e) {
    console.log(e);
    return response(ctx, {
      error: '대화방 생성 실패'
    });
  }

  response(ctx, {
    ok: true,
    payload: {
      meeting
    }
  });
};

export default CreateMeeting;