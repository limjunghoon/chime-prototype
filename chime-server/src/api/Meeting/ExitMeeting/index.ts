import {response} from "../../../lib/util";
import { QueryExitAttendee } from "../../Attendee/ExitAttendee";
import {TEST_SESSION} from "../../../config";

const ExitMeeting = async (ctx) => {
  const { request: { body: {
    interview_idx = ctx.request.body.interview_idx,
    attendeeId = ctx.request.body.attendeeId,
    ssid = ctx.request.body.ssid
  }}} = ctx;

  if (
    !(await QueryExitAttendee({ interview_idx, attendeeId }))
  ) return response(ctx, { error: '대화방 나가기 실패' });

  return response(ctx, {
    ok: true,
    payload: {}
  });
};

export default ExitMeeting;