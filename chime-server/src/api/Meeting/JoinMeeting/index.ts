import { response } from "../../../lib/util";
import Attendee from "../../../entities/Attendee";
import {QueryGetSession} from "../../Auth/VerifySession";

const InsertAttendee = async ({
  interview_idx,
  hr_manager_info,
  mcom_idx,
  mem_idx,
  user_name,
  interview_dt,
  interview_start_time,
  interview_end_time,
  send_mail_cnt,
  send_sms_cnt,
  user_phone_num,
  user_email,
  photo_src,
  last_school_nm,
  memo
}) => {
  let insertedAttendee;
  try {
    const attendee = new Attendee();
    attendee.interview_idx = interview_idx;
    attendee.hr_manager_info = hr_manager_info;
    attendee.mcom_idx = mcom_idx;

    attendee.mem_idx = mem_idx;
    attendee.user_name = user_name;
    attendee.interview_dt = interview_dt;
    attendee.interview_start_time = interview_start_time;
    attendee.interview_end_time = interview_end_time;
    attendee.send_mail_cnt = send_mail_cnt;
    attendee.send_sms_cnt = send_sms_cnt;
    attendee.user_phone_num = user_phone_num;
    attendee.user_email = user_email;
    attendee.photo_src = photo_src;
    attendee.last_school_nm = last_school_nm;
    attendee.memo = memo;

    insertedAttendee = await attendee.save();
  } catch(e) {
    console.log(e);
  }
  return insertedAttendee;
}

const JoinMeeting = async (ctx) => {
  const { request: { body: {
    ssid,
    interview_idx,
    hr_manager_info,
    mcom_idx,
    mem_idx,
    user_name,
    interview_dt,
    interview_start_time,
    interview_end_time,
    send_mail_cnt,
    send_sms_cnt,
    user_phone_num,
    user_email,
    photo_src,
    last_school_nm,
    memo
  }}} = ctx;

  if (!interview_idx || !ssid ) {
    return response(ctx, {
      error: `interview_idx: ${interview_idx}, ssid: ${ssid}`
    });
  }

  const session = await QueryGetSession(ssid);
  if (!session || !(session as any).mem_idx) return response(ctx, {
    error: '사림인 로그인 후 이용 하실 수 있습니다.'
  });

  if (!session.mcom_idx || session.mem_gb !== 'c') {
    return response(ctx, {
      error: '기업회원이 아니면 개인회원을 대화방에 참여시킬 수 없습니다.'
    });
  }

  const attendee = await InsertAttendee({
    interview_idx,
    hr_manager_info,
    mcom_idx,
    mem_idx,
    user_name,
    interview_dt,
    interview_start_time,
    interview_end_time,
    send_mail_cnt,
    send_sms_cnt,
    user_phone_num,
    user_email,
    photo_src,
    last_school_nm,
    memo
  });
  if (!attendee) return console.log('초대 실패');

  return response(ctx, {
    ok: true,
    payload: {
      attendee
    }
  });
};

export default JoinMeeting;
export {
  InsertAttendee
}