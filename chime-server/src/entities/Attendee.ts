import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

@Entity({ name: "interviewee" })
class Attendee extends BaseEntity {
  @PrimaryGeneratedColumn()
  interviewee_idx: number;

  @Column()
  interview_idx: number;

  // chime_test
  @Column({ type: 'text' })
  hr_manager_info: string;

  @Column()
  mcom_idx: number;

  @Column()
  mem_idx: number;

  @Column({ type: 'text' })
  user_name: string;

  @Column({ type: 'text' })
  interview_dt: string;

  @Column({ type: 'text' })
  interview_start_time: string;

  @Column({ type: 'text' })
  interview_end_time: string;

  @Column()
  send_mail_cnt: number;

  @Column()
  send_sms_cnt: number;

  @Column({ type: 'text' })
  user_phone_num: string;

  @Column({ type: 'text'})
  user_email: string;

  @Column({ type: 'text'})
  photo_src: string;

  @Column({ type: 'text'})
  last_school_nm: string;

  @Column({ type: 'text'})
  memo: string;

  @CreateDateColumn()
  reg_dt: string;

  @UpdateDateColumn()
  update_ts: string;
}

export default Attendee;