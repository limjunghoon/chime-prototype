import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

@Entity({ name: "interview_contents_template"})
class Meeting extends BaseEntity {
  @PrimaryGeneratedColumn()
  interview_contents_template_idx: number;

  @Column()
  mcom_idx: number;

  // contents_type = chime_test
  @Column({ type: "text"})
  contents_type: string;

  // { meetingId, meetingInfo }
  @Column({ type: "text"})
  contents_template: string;

  @CreateDateColumn()
  reg_dt: string;

  @UpdateDateColumn()
  update_ts: string;

  // interview_idx
  @Column({ type: "text"})
  template_key: string;
}

export default Meeting;

