import Koa from 'koa';

interface Response {
  ok?: boolean
  error?: string,
  payload?: any
}
export function response(ctx: Koa.BaseContext, response: Response = {
  ok: false,
  error: '',
  payload: null
}) {
  ctx.body = response;
}
