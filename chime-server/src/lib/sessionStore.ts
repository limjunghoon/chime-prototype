import Redis from 'ioredis';
import { unserialize } from 'php-serialize';
import { CONFIG_SRAMIN_REDIS } from "../config";

class SessionStore {
  store: Redis;

  constructor() {
    this.store = new Redis(CONFIG_SRAMIN_REDIS);
  }

  async get(key): Promise<any> {
    let result = {};

    let white = 100;
    try {
      let storage = [];

      let serializedSession = await this.store.get(key);
      // data = PHPUnserialize.unserializeSession(session);

      let offset = 0;
      while(offset < serializedSession.length) {
        // if (!serializedSession.substring(offset).indexOf('|')) break;

        const nameIdx = serializedSession.indexOf('|');
        const name = serializedSession.substring(nameIdx, -1);
        let other = serializedSession.substring(nameIdx + 1);

        let firstOpen = false;
        let openCnt = 0;
        let point = 0;

        for (let i = 0; i<other.length; i++) {
          if (firstOpen && !openCnt) {
            point = i;
            break;
          }

          if (other[i] === '{') {
            if (!firstOpen) {
              firstOpen = true;
            }
            openCnt += 1;
          }

          if (other[i] === '}') {
            openCnt -= 1;
          }
        }

        let data = other.substring(point, 0);

        try {
          storage[name] = unserialize(data);
        } catch (e) {
          console.log('unSerialize error...');
          console.log(e);
        }

        offset = name.length + data.length;
        serializedSession = serializedSession.slice(offset + 1);
      }

      if ((storage as any).Zend_Auth) {
        if ((storage as any).Zend_Auth.storage) {
          result = (storage as any).Zend_Auth.storage;
        }
      }
    } catch(e) {
      console.log(e);
    }

    return result;
  }
}

export default new SessionStore();