import Koa from "koa";
import bodyParser from "koa-bodyparser";
import cors from 'koa-cors';
import render from "./middleware/render";
import redirect from "./middleware/redirect";
import { createConnection } from "typeorm";
import cookieParser from 'koa-cookie';
import Router from 'koa-router';
import api from "./api";
import { morganMiddleware, logger } from "./middleware/logger";
import { CONFIG_SARAMIN_DATABASE } from "./config";
import { accessValidator } from "./middleware/validator";
import mount from 'koa-mount';

createConnection(CONFIG_SARAMIN_DATABASE).then(_ => {
  const app = new Koa();
  const router = new Router();

  app.use(cors());
  app.use(morganMiddleware);
  app.use(bodyParser());
  app.use(cookieParser());
  app.use(render());
  app.use(accessValidator);

  router.use('/api', api.routes());
  app.use(router.routes()).use(router.allowedMethods());

  app.use(redirect);

  const port = 3000;
  app.listen(port, () => {
    console.log(`listen ${port}`);
  });
}).catch(err => console.log(err));
