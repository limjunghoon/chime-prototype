import dotenv from 'dotenv';
import { ConnectionOptions } from 'typeorm'
import path from "path";

dotenv.config({
  path: path.join(__dirname, `../${process.env.NODE_ENV === 'production' ? '.env' : '.env.dev'}`)
});

const {
  DB_PORT,
  DB_ENDPOINT,
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_DB,
  REDIS_PASSWORD,
  REDIS_KEY_PREFIX,
  SESSION_TEST
} = process.env;

const CONFIG_SRAMIN_REDIS = {
  host: REDIS_HOST,
  port: REDIS_PORT,
  db: REDIS_DB,
  password: REDIS_PASSWORD,
  keyPrefix: REDIS_KEY_PREFIX
};

const CONFIG_SARAMIN_DATABASE: ConnectionOptions = {
  type: "mysql",
  database: DB_NAME,
  synchronize: false, // todo warning error !!!!! db 테이블 동기화 됨 사용금지
  logging: true,
  entities: [
    __dirname + '/entities/*{.ts,.js}'
  ],
  host: DB_ENDPOINT,
  port: parseInt(DB_PORT, 10),
  username: DB_USERNAME,
  password: DB_PASSWORD
};

const CLIENT_BUILD_PATH = path.join(__dirname, '../../chime-client/build');

const TEST_SESSION = SESSION_TEST ? {
  mem_idx: 11111,
  mcom_idx: "",
  mem_gb: 'p',
  interview_idx: 5,
  ssid: 'ssid51',
  attendeeId: "7fcc4cef-8a2d-4429-86c4-40f7a41f31a6"
} : null;

export {
  CONFIG_SRAMIN_REDIS,
  CONFIG_SARAMIN_DATABASE,
  CLIENT_BUILD_PATH,
  TEST_SESSION
}