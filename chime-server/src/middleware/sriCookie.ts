const setSriCookie = async (ctx, next) => {
  const { query: { ssid }} = ctx;
  console.log('set cookie');
  console.log(ssid);

  if (ssid) {
    ctx.cookies.set('ssid', ssid);
  }
  await next();
};

export default setSriCookie;