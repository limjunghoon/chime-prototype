import * as path from "path";
import serve from 'koa-static';

import { CLIENT_BUILD_PATH } from "../config";

export default () => {
  return serve(CLIENT_BUILD_PATH);
};