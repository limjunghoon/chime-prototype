import * as path from "path";
import send from 'koa-send';
import { CLIENT_BUILD_PATH } from "../config";

export default async (ctx, next) => {
  if(ctx.status !== 404) return next();

  await send(ctx, 'index.html', {
    root: CLIENT_BUILD_PATH
  });
}